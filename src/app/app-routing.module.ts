import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewFeedItemComponent } from './view-feed-item/view-feed-item.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [{ path: '', redirectTo: '/viewfeed', pathMatch: 'full' },{ path: 'viewfeed', component: ViewFeedItemComponent },{ path: 'Nav', component: NavbarComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
