import {Component, OnInit } from '@angular/core';
import { FeeditemService } from './../_shared/feeditem.service';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { SearchkeyService } from '../_shared/searchkey.service'

@Component({
  selector: 'app-view-feed-item',
  templateUrl: './view-feed-item.component.html',
  styleUrls: ['./view-feed-item.component.scss']
})
export class ViewFeedItemComponent implements OnInit {

  FeedItem: any = [];//feed item array 
  LoadingDataStatus: boolean = false//loagin icon show or hide status
  
  
  constructor(
    public _FeeditemService: FeeditemService,//feed item service
    public _NgxXml2jsonService: NgxXml2jsonService,//servuce for convert xml to jason
    public _SearchkeyService:SearchkeyService,//serch and filter service
  ) {


  }

 
// load data from service
  getFeed(value:any) {

    this._FeeditemService.getFeed(value).subscribe(Result => {//call http request from service.
      const parser = new DOMParser();
      const xml = parser.parseFromString(Result.toString(), 'text/xml')//convet xml data to jason
      this.FeedItem = this._NgxXml2jsonService.xmlToJson(xml);
      this.LoadingDataStatus = true;
     
    })
  }

  ngOnInit() {
    
    this.getFeed("");//load all data at first intializing time
  
    //filter at search box key change, observe event emiter
    this._SearchkeyService.change.subscribe( key => {
      this.LoadingDataStatus = false;
      this.getFeed(key);
    });

  }



}
