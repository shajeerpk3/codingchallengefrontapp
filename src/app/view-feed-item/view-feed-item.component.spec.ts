import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFeedItemComponent } from './view-feed-item.component';

describe('ViewFeedItemComponent', () => {
  let component: ViewFeedItemComponent;
  let fixture: ComponentFixture<ViewFeedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFeedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFeedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
