import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

//used for pass value from nav bar to feed item component. we are using event emitter for this operation

export class SearchkeyService {
  key = ""
  @Output() change: EventEmitter<any> = new EventEmitter();

  public Search(key:any) {
    this.key = key;
    this.change.emit(this.key);//add key word  to emitter for serching
  }
  constructor() { }
}
