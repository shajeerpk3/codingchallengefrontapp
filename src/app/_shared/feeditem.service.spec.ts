import { TestBed } from '@angular/core/testing';

import { FeeditemService } from './feeditem.service';

describe('FeeditemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeeditemService = TestBed.get(FeeditemService);
    expect(service).toBeTruthy();
  });
});
