import { TestBed } from '@angular/core/testing';

import { SearchkeyService } from './searchkey.service';

describe('SearchkeyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchkeyService = TestBed.get(SearchkeyService);
    expect(service).toBeTruthy();
  });
});
