import { Injectable } from '@angular/core';
import { HttpClient,  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
//http request for get feed item. using angular httpclient

export class FeeditemService {
  constructor(public httpClient: HttpClient) { }
  
  public getFeed(key) {
    return this.httpClient.get(`http://localhost:3000/getfeed?key=`+ key);
  }


}
