import { Component, OnInit } from '@angular/core';
import { SearchkeyService } from '../_shared/searchkey.service'
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(public _SearchkeyService: SearchkeyService) { }

  onChange(event: any) {
    this._SearchkeyService.Search(event.target.value)// send value to sevice for searching.
  }
  ngOnInit() {
  }

}
